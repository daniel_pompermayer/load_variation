# Load Variation Python Module

## Introduction
Load Variation is a python module developed for simulate thee-phase electrical loads and compute each of its phase complex power.

This project is discontinued. Please take a look at [Fidelia Project](https://gitlab.com/daniel_pompermayer/fidelia).

## Build with
 - Emacs
 - Python

## Development Status
Load Variation module is fully running for Delta-connected loads. Wye-connected loads seem to present some weird behavior, which must be confirmed.

## Needed Improvements
This code has been built as a proof of concept. It may need some standardization or optimization. You may feel free to contribute.

## Getting Started
In order to run Load Variation module, all you need to do is download load_variation.py source file, import it under some python script and call its functions passing the needed arguments.

You may also run it as is directly from terminal, which will run a delta-connected load simulation, sweeping loads with 11 modulus values and 15 argument values of admittance.

Pay attention to the "../Imagens/simulacao_delta.png" clause in the 134th line, which means that your scatter plot will be named "simulacao_delta.png" and will be saved into a directory called "Imagens" that will be created into the parent directory of the current one.

## Authors
 - Daniel Campos Pompermayer

## License
Copyright 2018 Daniel Campos Pompermayer

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.