#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Compute the power of a range of three-phase loads."""
__author__ = "Daniel Campos Pompermayer"

import numpy as np
import itertools as it
import pandas as pd
import matplotlib.pyplot as plt


def generate_sets_of_admittances(num_of_moduli, num_of_arguments):
    """Build some complex numbers and generate three-phase sets of
    addmitances."""
 
    # Declare arrays containing the indicated number of moduli and arguments.
    moduli = np.linspace(0, 1, num_of_moduli)
    arguments = np.linspace(-np.pi/2, np.pi/2, num_of_arguments)
 
    # Pair moduli and arguments and generate some comple numbers.
    pairs = it.product(moduli, arguments)
    complex_numbers = [mod * np.exp(1j * arg) for mod, arg in pairs]
 
    # Combine it exhaustively three by three.
    return np.array(list(set(it.product(complex_numbers, repeat=3))))
 
 
def delta_load(ya_yb_yc):
    """Build the matrix of admittances of a delta-connected load."""

    ya, yb, yc = ya_yb_yc
    return np.array([[(ya + yc), -ya, -yc],
                     [-ya, (ya + yb), -yb],
                     [-yc, -yb, (yb + yc)]])
 
 
def wye_load(ya_yb_yc):
    """Build the matrix of admittances of a wye-connected load."""

    ya, yb, yc = ya_yb_yc
    return np.array([[ya, 0, 0, -ya],
                     [0, yb, 0, -yb],
                     [0, 0, yc, -yc],
                     [-ya, -yb, -yc, (ya+yb+yc)]])


def neutral_voltage(Y):

    # Rotating Phasor.
    alfa = -0.5+0.8660254037844386j

    # Array of voltages.
    V = [1, alfa**2, alfa]

    #       (Ya + a².Yb + a.Yc)       diag(Y) x V
    # Vn = --------------------- = ---------------
    #          Ya + Yb + Yc           sum(diag(Y))
    # We must take care when sum(diag(Y)) is zero to avoid warnings.
    if sum(np.diag(Y)) == 0:
        vn = 0
    else:
        vn = np.diag(Y).dot(V)/sum(np.diag(Y))
    return vn


def complex_power(Y, topology="delta", neutral_grounding=False):
    """Compute the complex power of each phase of a load from its matrix of
    admittances."""

    # Rotating Phasor.
    alfa = -0.5+0.8660254037844386j

    # Array of voltages.
    V = [1, alfa**2, alfa]

    # Compute Power.
    # S = V.(YxV)*
    S = V * np.conjugate(Y[..., 0:3, 0:3].dot(V))

    # Check whether we must compute power due to the neutral voltage.
    if topology is "wye" and not neutral_grounding:

        # Since each load has its own neutral voltage, I must compute it
        # individually.
        if Y.ndim < 3:
            vn = neutral_voltage(Y[0:3])
            S += Y[0:3, -1] * vn
        else:
            vn = np.array(
                [neutral_voltage(Y[n, 0:3]) for n in range(Y.shape[0])])
            S += Y[..., 0:3, -1] * vn[:, np.newaxis]
 
    return S
 
 
def plot_sa_power(powers, output_file):
    """Drawn a scatter plot over a Argand plan to better present
    the power data."""
 
    # Instance a new figure.
    plt.figure()
 
    # Create a pandas data frame to better index and slice the data.
    df = pd.DataFrame(powers)
 
    # Rename the columns to some more appropriated names.
    df = df.rename(columns={0: "Sa", 1: "Sb", 2: "Sc"})
 
    # Slice the Sa power into the 4 quadrants of the Argand Plan.
    neg_cap = df[np.all([np.real(df.Sa) <= 0, np.imag(df.Sa) < 0], axis=0)].Sa
    pos_cap = df[np.all([np.real(df.Sa) > 0, np.imag(df.Sa) < 0], axis=0)].Sa
    pos_ind = df[np.all([np.real(df.Sa) > 0, np.imag(df.Sa) >= 0], axis=0)].Sa
    neg_ind = df[np.all([np.real(df.Sa) <= 0, np.imag(df.Sa) >= 0], axis=0)].Sa
 
    # Scatter plot size.
    mark_size = 0.1
 
    # Name labels.
    plt.xlabel("P (p.u.)")
    plt.ylabel("Q (p.u.)")
 
    # Plot each quadrant.
    plt.scatter(np.real(neg_ind), np.imag(neg_ind), s=mark_size, c="r",
                label="Negative Active Power\n with Lagging Power Factor")
    plt.scatter(np.real(pos_ind), np.imag(pos_ind), s=mark_size, c="navy",
                label="Positive Active Power\n with Lagging Power Factor")
    plt.scatter(np.real(neg_cap), np.imag(neg_cap), s=mark_size, c="tomato",
                label="Negative Active Power\n with Leading Power Factor")
    plt.scatter(np.real(pos_cap), np.imag(pos_cap), s=mark_size, c="steelblue",
                label="Positive Active Power\n with Lagging Power Factor")
 
    # Insert some fake axes.
    # plt.arrow(-4, 0, 8, 0, head_width=0.3, head_length=0.3, fc='k', ec='k')
    # plt.arrow(0, -4, 0, 7.65, head_width=0.3, head_length=0.3, fc='k', ec='k')
    # plt.axis("equal")
    # plt.axis([-4, 4, -4, 4])
 
    # # Insert legend on the lower left corner.
    # plt.legend(loc="lower left", markerscale=20)
     
    # Save the plot.
    plt.savefig(output_file, bbox_inches="tight")
    
    
# Script section
if __name__ == "__main__":
    
    # Generate sets of admittances.
    sets_of_admittances = generate_sets_of_admittances(
        num_of_moduli=11, num_of_arguments=15)
 
    # Generate some loads.
    delta_loads = np.apply_along_axis(delta_load, 1, sets_of_admittances)
     
    # Compute the powers.
    delta_powers = complex_power(delta_loads)
     
    # Plot the Sa powers.
    plot_sa_power(delta_powers,
                  "../Texto/Imagens/Simulações/simulacao_delta.png")
